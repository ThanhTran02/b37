import React from 'react'
import { Form, Table } from '../components'

const Home = () => {
    return (
        <div className='container px-10 mx-auto'>
            <div className=' p-5'>
                <h1 className='text-3xl font-mono font-bold'>BT Form </h1>
            </div >
            <Form />
            <Table />
        </div >
    )
}

export default Home