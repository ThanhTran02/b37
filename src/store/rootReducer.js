import { combineReducers } from "@reduxjs/toolkit";
import { btFormReducer } from "./formReducer/slice";

export const rootReducer = combineReducers({
    btForm: btFormReducer
})