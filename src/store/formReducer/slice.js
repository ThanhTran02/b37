import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  productList: [],
  productEdit: undefined,
};

const btFormSlice = createSlice({
  name: "btForm",
  initialState,
  reducers: {
    add: (state, action) => {
      state.productList.push(action.payload);
    },
    delete: (state, action) => {
      state.productList = state.productList.filter(
        (item) => item.id !== action.payload
      );
    },
    edit: (state, action) => {
      state.productEdit = action.payload;
    },
    submissEdit: (state, action) => {
      state.productList = state.productList.map((item) => {
        if (item.id === action.payload.id) {
          return action.payload;
        }
        return item;
      });
      state.productEdit = undefined;
    },
  },
});

export const { actions: btFormActions, reducer: btFormReducer } = btFormSlice;
