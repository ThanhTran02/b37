import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { btFormActions } from "../store/formReducer/slice";

const Table = () => {
  const { productList } = useSelector((state) => state.btForm);
  const dispatch = useDispatch();
  return (
    <table className="table-fixed w-full mt-10">
      <thead>
        <tr className="bg-black text-white ">
          <th className="py-3">Mã Sv</th>
          <th className="">Họ tên</th>
          <th className="">Số điện thoại</th>
          <th className="">Email</th>
          <th className="">Action</th>
        </tr>
      </thead>
      <tbody>
        {productList.map((item) => (
          <tr key={item?.id}>
            <td>{item?.id}</td>
            <td>{item?.name}</td>
            <td>{item?.numberPhone}</td>
            <td>{item?.email}</td>
            <td className="text-center">
              <button
                type="button"
                className="focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900"
                onClick={() => dispatch(btFormActions.delete(item.id))}
              >
                Delete
              </button>
              <button
                type="button"
                className="focus:outline-none text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800"
                onClick={() => dispatch(btFormActions.edit(item))}
              >
                Edit
              </button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default Table;
