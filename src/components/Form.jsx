import React, { useEffect, useState } from "react";
import { useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { btFormActions } from "../store/formReducer/slice";

const Form = () => {
  const [formData, setFormData] = useState();
  const [formErr, setFormErr] = useState("");
  const formRef = useRef(null);
  const dispatch = useDispatch();

  const { productEdit } = useSelector((state) => state.btForm);

  const handleReset = () => {
    formRef.current.reset();
  };

  const handleData = () => (e) => {
    const { name, value, pattern, minLength, validity } = e.target;
    let mess = "";
    if (minLength !== -1 && !value.length) {
      mess = "Vui lòng nhập thêm thông tin!";
    } else if (
      validity.patternMismatch &&
      ["id", "numberPhone"].includes(name)
    ) {
      mess = "Vui lòng nhập giá trị là số!";
    } else if (validity.patternMismatch && name === "email") {
      mess = "Vui lòng nhập đúng email!";
    }
    setFormErr({
      ...formErr,
      [name]: mess,
    });
    setFormData({ ...formData, [name]: mess ? undefined : value });
  };
  useEffect(() => {
    if (!productEdit) return;
    setFormData(productEdit);
  }, [productEdit]);

  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();

        let formErr = {};
        const elements = document.querySelectorAll("input");
        {
          elements.forEach((e) => {
            const { name, value, minLength, validity } = e;
            let mess = "";
            if (minLength !== -1 && !value.length) {
              mess = "Vui lòng nhập thêm thông tin!";
            } else if (
              validity.patternMismatch &&
              ["id", "numberPhone"].includes(name)
            ) {
              mess = "Vui lòng nhập giá trị là số!";
            } else if (validity.patternMismatch && name === "email") {
              mess = "Vui lòng nhập đúng email!";
            }
            formErr[name] = mess;
          });
        }
        let flag = false;
        for (let key in formErr)
          if (formErr[key]) {
            flag = true;
            break;
          }
        if (flag) {
          setFormErr(formErr);
          return;
        }
        if (productEdit) {
          dispatch(btFormActions.submissEdit(formData));
        } else {
          dispatch(btFormActions.add(formData));
        }
        setFormData("");
      }}
      ref={formRef}
      noValidate
    >
      <h2 className="bg-black text-white p-5 text-start text-2xl  font-bold">
        Thông tin sinh viên
      </h2>
      <div className="grid grid-cols-2 gap-4 mt-4">
        <div className="">
          <p>Mã SV</p>
          <input
            type="text"
            placeholder="Nhập mã sinh viên"
            name="id"
            pattern="^[0-9]+$"
            minLength={5}
            maxLength={10}
            value={formData?.id}
            disabled={!!productEdit}
            onChange={handleData()}
          />
          <p className="text-red-600 text-xl">{formErr?.id}</p>
        </div>
        <div>
          <p>Họ tên</p>
          <input
            type="text"
            placeholder="Nhập họ tên"
            name="name"
            minLength={1}
            value={formData?.name}
            onChange={handleData()}
          />
          <p className="text-red-600 text-xl">{formErr?.name}</p>
        </div>
        <div>
          <p>Số điện thoại</p>
          <input
            type="text"
            placeholder="Nhập số điện thoại"
            name="numberPhone"
            pattern="^[0-9]+$"
            minLength={1}
            value={formData?.numberPhone}
            onChange={handleData()}
          />
          <p className="text-red-600 text-xl">{formErr?.numberPhone}</p>
        </div>
        <div>
          <p>Email</p>
          <input
            type="text"
            placeholder="Nhập Email"
            name="email"
            minLength={1}
            pattern="[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,}$"
            value={formData?.email}
            onChange={handleData()}
          />
          <p className="text-red-600 text-xl">{formErr?.email}</p>
        </div>
      </div>
      {!productEdit ? (
        <button
          className=" px-4 py-2 bg-green-500 mt-5 rounded-md text-lg text-white font-semibold hover:bg-slate-100 hover:text-green-600 transition"
          onClick={handleReset}
        >
          Thêm sinh viên
        </button>
      ) : (
        <button
          class="mt-5 text-white bg-purple-700 hover:bg-purple-800 focus:outline-none focus:ring-4 focus:ring-purple-300  font-semibold rounded-md text-lg px-4 py-2 text-center mb-2 dark:bg-purple-600 dark:hover:bg-slate-200 dark:focus:ring-purple-900 hover:text-purple-700 transition"
          onClick={handleReset}
        >
          Update
        </button>
      )}
    </form>
  );
};

export default Form;
